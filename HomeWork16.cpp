﻿#include <iostream>
using namespace std;







int main()
{
    // Вариант 1



    /*

    const int N = 5;
    int date;
    cout << "What is today’s date?\n";
    cin >> date;
    int ArrayString = date % N;



    int array[N][N];
    int sum;

    cout << "The resulting array:\n";
    for (int i = 0; i < N; i++)
    {

        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            cout << array[i][j];

            if (i == ArrayString)
            {
                // По условию задания все строки и столбцы массива являются арифметическими прогрессиями:

                sum = ((array[i][0]) + (array[i][j])) * (N) / 2;


            }
        }

        cout << '\n';

    }

    cout <<"Sum of "<< ArrayString<<" string:   "<<sum;
    cout << '\n';

   */




    // Вариант 2




   /*
    const int N = 5;
    int date;
    cout << "What is today’s date?\n";
    cin >> date;

    int ArrayString = date % N;

    int array[N][N];

    cout << "The resulting array:\n";
    for (int i = 0; i < N; i++)
    {

        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            cout << array[i][j];
        }

        cout << '\n';

    }

    int sum = 0;

    for (int k = 0; k < N; k++)
    {
        sum += array[ArrayString][k];
    }

    cout << '\n' <<sum;

    */



    // Вариант 2 с динамическим массивом
    
    int N;
    cout << "Enter the value:\n";
    cin >> N;

    int date;
    cout << "What is today’s date?\n";
    cin >> date;

    int ArrayString = date % N;


   

    int** array;
    array = new int* [N];

    for (int i = 0; i < N; i++)
    {
        array[i] = new int[N];
    }
    


    cout << "The resulting array:\n";
    for (int i = 0; i < N; i++)
    {

        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            cout << array[i][j];
        }

        cout << '\n';

    }

    int sum = 0;

    for (int k = 0; k < N; k++)
    {
        sum += array[ArrayString][k];
    }

    cout << '\n' << sum;

    for (int i = 0; i < N; i++)
    {
        delete[] array[i];
    }

    delete[] array;

    return 0;
}

